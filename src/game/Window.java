package game;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.Random;
import javax.swing.JFrame;
import javax.swing.Timer;

/**
 * Okno ve kterem se vola update collideru, controlleru, generuji se mraky a vykresluje hra.
 * @author Zuzana Vilhelmova
 */
public class Window extends JFrame implements ActionListener, ComponentListener {

  private Image buffer;
  private Stats stats = null;
  private Menu menu = null;
  private Help help = null;
  private Controller controller = null;
  private Collider collider = null;
  private static float framesPerSecond = 120.000f;
  private long lastPaint = System.nanoTime();
  private long averagePaint = 1000000;
  Timer timer;
  private Color color;
  private boolean start = false;
  private boolean showHelp = false;
  private Random random;

  public Window(String title) {
    super(title);
    menu = new Menu();
    help = new Help();
    stats = new Stats();
    random = new Random();
    timer = new Timer(5, this);
    controller = new Controller();
    collider = new Collider();
    setSize(800, 600);
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    setLayout(new BorderLayout());
    setFocusable(true);
    addComponentListener(this);

    addMouseListener(menu);
    addMouseMotionListener(menu);
    buffer = createImage(getWidth(), getHeight());
    setVisible(true);
    timer.start();
  }

  public Controller getController() {
    return controller;
  }

  /**
   * Nacte hru, pripadne vlastni nahrany level.
   */
  public void startGame() {
    addKeyListener(controller);
    addMouseListener(stats);
    addMouseMotionListener(stats);
    removeMouseListener(menu);
    removeMouseMotionListener(menu);
    start = true;
    if (controller.isQuit()) {
      controller.setQuit(false);
    }
    if (Game.map.isOwnLevel()) {
      Game.map.setLevel(0);
    } else {
      Game.map.setLevel(1);
    }
    Game.map.setEnd(false);
    Game.map.reset();
  }

  public Stats getStats() {
    return stats;
  }

  public Menu getMenu() {
    return menu;
  }

  /**
   * Zobrazi help
   */
  public void startHelp() {
    removeMouseListener(menu);
    removeMouseMotionListener(menu);
    addMouseListener(help);
    addMouseMotionListener(help);
    showHelp = true;
  }

  /**
   * Vypne help a zobrazi zpet menu.
   */
  public void quitHelp() {
    removeMouseListener(help);
    removeMouseMotionListener(help);
    addMouseListener(menu);
    addMouseMotionListener(menu);
    showHelp = false;
  }

  /**
   * odstrani KeyListener v pripade prohry, vyhry, pauzy, nebo zastaveni prez
   * escape.
   */
  public void end() {
    removeKeyListener(controller);
  }

  /**
   * Zapne hru zpet po pauze, escapu nebo vyhry.
   */
  public void start() {
    addKeyListener(controller);
  }

  /**
   * Zkonci hru a zobrazi menu.
   */
  public void endGame() {
    removeMouseListener(stats);
    removeMouseMotionListener(stats);
    removeKeyListener(controller);
    addMouseListener(menu);
    addMouseMotionListener(menu);
    Game.map.setEnd(true);
    Game.map.setOwnLevel(false);
    start = false;
  }

  @Override
  public synchronized void paint(Graphics gr) {
    long timeToPaint = System.nanoTime() - lastPaint;

    double newAveragePaint = (averagePaint / 200.0) * 199.0 + (double) timeToPaint / 200.0;

    averagePaint = (long) newAveragePaint;
    lastPaint = System.nanoTime();

    framesPerSecond = (float) (1000000000.0 / averagePaint);

    if (buffer != null) {
      Graphics g = buffer.getGraphics();
      color = new Color(78, 172, 235);
      Game.map.setOffsetY(gr.getClipBounds().height - 608);
      g.setColor(color);
      g.fillRect(0, 0, gr.getClipBounds().width, gr.getClipBounds().height);
      if (Game.map != null && start) {
        Game.map.paint(g);
//          System.out.println("offsetX: " + Game.map.getOffsetX());
//          System.out.println("offsetY: " + Game.map.getOffsetY());
      }
      if (start) {
        for (Figure figure : Game.map.figures) {
          figure.paint(g);
        }
//                g.setColor(Color.WHITE);
//                g.drawString(Integer.toString((int) framesPerSecond) + " FPS", 120, 50);
        stats.paint(g);
      } else {
        if (showHelp) {
          help.paint(g);
        } else {
          menu.paint(g);
        }
      }
      gr.drawImage(buffer, 0, 0, this);

    }

  }

  @Override
  public void actionPerformed(ActionEvent e) {
    if (random.nextInt((int) (framesPerSecond * 2)) == 0 && !controller.isPause() && !controller.isQuit()) {
      Game.map.clouds.add(new Cloud(-100, 200 - random.nextInt(Game.window.getHeight() / 2), random.nextInt(4)));
    }
    controller.update();
    collider.update();
    repaint();
  }

  @Override
  public void componentResized(ComponentEvent e) {
    buffer = createImage(getWidth(), getHeight());

  }

  @Override
  public void componentMoved(ComponentEvent e) {
  }

  @Override
  public void componentShown(ComponentEvent e) {
  }

  @Override
  public void componentHidden(ComponentEvent e) {
  }

  public static float getFPS() {
    return framesPerSecond;
  }
}
