package game;

import java.awt.Graphics;
import java.awt.Image;

/**
 * Trida predstavujici blok mapy.
 * @author Zuzana Vilhelmova
 */
public class Block {

    protected Image image;
    public final static float BLOCK_SIZE = 40;

    public Block() {

    }

    public void paint(Graphics g, float posX, float posY) {
        g.drawImage(image, (int) posX, (int) posY, Game.window);
    }
}
