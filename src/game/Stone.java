package game;

import javax.swing.ImageIcon;

/**
 * Blok predstavujici kamen.
 * @author Zuzana Vilhelmova
 */
public class Stone extends Block {

  private int posX, posY;
  static ImageIcon i = new ImageIcon(Dirt.class.getResource("stone.png"));

  public Stone() {
    image = i.getImage();
  }
}
