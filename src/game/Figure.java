package game;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.List;

/**
 * Abstraktni trida vsech postavicek.
 * @author Zuzana Vilhelmova
 */
public abstract class Figure {

    protected float x;
    protected float y;
    protected float sx;
    protected float sy;
    protected float gravity;
    protected List<BufferedImage> images;
    protected List<BufferedImage> rotatedImages;

    public Figure() {
    }

    public void fire() {
    }

    abstract void paint(Graphics g);

    abstract void hit();

    abstract void collision(Figure figure);

    abstract public void move(int mx, int my);

    public void setSizeAndPos(Rectangle a) {
        a.width = (int) sx;
        a.height = (int) sy;
        a.x = (int) x + (int) Game.map.getOffsetX();
        a.y = (int) y + (int) Game.map.getOffsetY();
    }

    /**
     * obrati smer postavicky
     * @param figure 
     */
    public void setDir(Figure figure) {
        if (figure instanceof Monster) {
            ((Monster) figure).dir *= -1;
        }
        if (figure instanceof Bird) {
            ((Bird) figure).dir *= -1;
        }
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getSx() {
        return sx;
    }

    public float getSy() {
        return sy;
    }
}
