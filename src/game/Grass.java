/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import javax.swing.ImageIcon;

/**
 * Blok predstavujici travu.
 * @author kiki
 */
public class Grass extends Block {
    
    static ImageIcon i = new ImageIcon(Dirt.class.getResource("grass.png"));

    public Grass() {
        image = i.getImage();
    }
    
    
    
}
