package game;

import java.awt.Image;
import javax.swing.ImageIcon;

/**
 * Blok predstavujici hlinu.
 * @author Zuzana Vilhelmova
 */
public class Dirt extends Block {
    

  static ImageIcon i = new ImageIcon(Dirt.class.getResource("dirt.png"));
  

  public Dirt() {
    image = i.getImage();
  }
   
}