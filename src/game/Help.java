package game;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

/**
 * Trida vykresluje navod ke hre.
 * @author Zuzana Vilhelmova
 */
public class Help implements MouseListener, MouseMotionListener {
    
    private Button buttonBack;
    private Point point;
    private int fontSize = 30;
    private int buttonSizeX = 100;
    private int buttonSizeY = 40;

    public Help() {
        buttonBack = new Button(buttonSizeX, buttonSizeY);
        point = new Point();
        point.x = 0;
        point.y = 0;
        buttonBack.setColor(Color.RED);
        buttonBack.setFontColor(Color.GREEN);
    }
    
    public void paint(Graphics g) {
        g.setColor(Color.ORANGE);
        g.setFont(new Font("Monospaced", Font.BOLD, 80));
        int w = g.getFontMetrics().stringWidth("HELP");
        g.drawString("HELP", Game.window.getWidth() / 2 - w / 2, 130);
        g.setColor(Color.YELLOW);
        
        g.setFont(new Font("Monospaced", Font.PLAIN, 30));
        String right = "Move right:    right arrow / D";
        int w1 = g.getFontMetrics().stringWidth(right);
        g.drawString(right , Game.window.getWidth() / 2 - w1 / 2, 200);
        
        String left = "Move left:    left arrow / A";
        int w2 = g.getFontMetrics().stringWidth(left);
        g.drawString(left , Game.window.getWidth() / 2 - w2 / 2, 250);
        
        String jump = "Jump:    space";
        int w3 = g.getFontMetrics().stringWidth(jump);
        g.drawString(jump , Game.window.getWidth() / 2 - w3 / 2, 300);
        
        String fire = "Fire:    Ctrl";
        int w4 = g.getFontMetrics().stringWidth(fire);
        g.drawString(fire , Game.window.getWidth() / 2 - w4 / 2, 350);
        
        g.setFont(new Font("Monospaced", Font.PLAIN, 20));
        String file = "If you load own level, you can change back to default.";
        String file2 = "After playing own level it resets back to default.";
        int w5= g.getFontMetrics().stringWidth(file);
        g.drawString(file , Game.window.getWidth() / 2 - w5 / 2, 400);
        int w6= g.getFontMetrics().stringWidth(file2);
        g.drawString(file2 , Game.window.getWidth() / 2 - w6 / 2, 430);
        
        buttonBack.paint(g, Game.window.getWidth() / 2 - buttonSizeX / 2, Game.window.getHeight() - 130, "BACK", fontSize);
    }
    
    @Override
    public void mouseClicked(MouseEvent e) {
        point.x = e.getX();
        point.y = e.getY();
        
        if(buttonBack.getRec().contains(point)){
            Game.window.quitHelp();
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mouseDragged(MouseEvent e) {
    }

    @Override
    public void mouseMoved(MouseEvent e) {
                point.x = e.getX();
        point.y = e.getY();

        if (buttonBack.getRec().contains(point)) {
            buttonBack.setColor(Color.YELLOW);
            buttonBack.setFontColor(Color.RED);
        } else {
            if (buttonBack.getColor() != Color.RED) {
                buttonBack.setColor(Color.RED);
                buttonBack.setFontColor(Color.GREEN);
            }
        }
    }
    
}
