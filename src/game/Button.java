/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;

/**
 * Vlastni tlacitko.
 * @author kiki
 */
public class Button {

  private int sizeX;
  private int sizeY;
  private Rectangle rec;
  private Color color = Color.BLACK;
  private Color fontColor = Color.WHITE;

  public Button(int sizeX, int sizeY) {
    this.sizeX = sizeX;
    this.sizeY = sizeY;
    rec = new Rectangle(0, 0, sizeX, sizeY);
  }

  public void paint(Graphics g, float posX, float posY, String text, int fontSize) {
    rec.x = (int) posX;
    rec.y = (int) posY + 40;
    g.setColor(color);
    g.fillRect((int) posX, (int) (posY + 40), sizeX, sizeY);
    g.setColor(fontColor);
    g.setFont(new Font("Monospaced", Font.BOLD, fontSize));
    int w = g.getFontMetrics().stringWidth(text);
    int h = g.getFontMetrics().getHeight();
    g.drawString(text, (int) (posX + sizeX / 2 - w / 2), (int) (posY + sizeY / 2 + 40 + ((h / 4) * 3) / 2));
  }

  public void setColor(Color color) {
    this.color = color;
  }

  public void setFontColor(Color fontColor) {
    this.fontColor = fontColor;
  }

  public Color getColor() {
    return color;
  }

  public Color getFontColor() {
    return fontColor;
  }

  public Rectangle getRec() {
    return rec;
  }

  public int getSizeX() {
    return sizeX;
  }

  public int getSizeY() {
    return sizeY;
  }
}
