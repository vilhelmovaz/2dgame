package game;

/**
 * Trida zjistuje typ bloku, konec mapy, id preser a konec prisery
 * @author Zuzana Vilhelmova
 */
public class BlockType {

  /*
   * 0-vzduch
   * 1-hlina s travou
   * 2-hlina bez travy
   * 3-kamen
   * 4-ruby
   * 5-diamant
   * 6-hrac
   * 7-prisera
   * 8-ptak
   */
  public static int getType(long x) {
    return (int) (x % 256);
  }

  public static boolean isEnd(long x) {
    return ((x / 256) % 256) == 1;
  }

  public static int getMonsterId(long x) {
    return (int) (((x / 256) / 256) % 256);
  }

  public static boolean isMonsterEnd(long x) {
    return ((((x / 256) / 256) / 256) % 256) == 1;
  }
  /*
   * 1.byte=typ
   * 2.byte=priznak konce(0=nic,1-konec)
   * 3.byte=pokud je typ prisera-id
   * 4.byte=pokud je prisera,pro pocatek 0, pro konec 1
   */
}
