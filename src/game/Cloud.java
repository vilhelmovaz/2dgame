package game;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 * Nahodne generujici se mraky.
 * @author Zuziiik
 */
public class Cloud {

  static BufferedImage i1;
  static BufferedImage i2;
  static BufferedImage i3;
  private Image image;
  final static float speed = 50;
  private float leftX, leftY, rightX;

  static {
    try {
      i1 = ImageIO.read(Monster.class.getResource("cloud1.png"));
      i2 = ImageIO.read(Monster.class.getResource("cloud2.png"));
      i3 = ImageIO.read(Monster.class.getResource("cloud3.png"));
    } catch (IOException ex) {
      Logger.getLogger(Monster.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  public Cloud(float leftX, float leftY, int cloud) {
    this.leftX = leftX;
    this.leftY = leftY;
    this.rightX = Game.map.getMapWidth();
    switch (cloud) {
      case 1:
        image = i1;
        break;
      case 2:
        image = i2;
        break;
      case 3:
        image = i3;
        break;
    }
  }

  public float getLeftX() {
    return leftX;
  }

  public float getRightX() {
    return rightX;
  }

  void paint(Graphics g) {
    if (!Game.map.isEnd() && !Game.window.getController().isQuit()) {
      g.drawImage(image, (int) (leftX + Game.map.getOffsetX()), (int) (leftY + Game.map.getOffsetY()), Game.window);
    }
  }

  public void move() {
    leftX += speed / Window.getFPS();
  }
}
