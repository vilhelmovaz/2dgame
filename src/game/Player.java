package game;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 * Trida predstavujici hrace.
 * @author Zuzana Vilhelmova
 */
public class Player extends Figure {

  final static float speed = 200;
  private float speedY = 3;
  private float startX;
  private float startY;
  public int dir;
  private Rectangle rec;
  static BufferedImage i;
  static BufferedImage i1;
  static BufferedImage i2;
  static BufferedImage i3;
  static BufferedImage i4;
  static BufferedImage rotetedI;
  static BufferedImage rotetedI1;
  static BufferedImage rotetedI2;
  static BufferedImage rotetedI3;
  static BufferedImage rotetedI4;

  static {

    try {
      i = ImageIO.read(Player.class.getResource("rabbit.png"));
      i1 = ImageIO.read(Player.class.getResource("rabbit1.png"));
      i2 = ImageIO.read(Player.class.getResource("rabbit2.png"));
      i3 = ImageIO.read(Player.class.getResource("rabbit3.png"));
      i4 = ImageIO.read(Player.class.getResource("rabbit4.png"));
    } catch (IOException ex) {
      Logger.getLogger(Player.class.getName()).log(Level.SEVERE, null, ex);
    }
    rotetedI = getRotated(i);
    rotetedI1 = getRotated(i1);
    rotetedI2 = getRotated(i2);
    rotetedI3 = getRotated(i3);
    rotetedI4 = getRotated(i4);

  }

  private static BufferedImage getRotated(BufferedImage normalImage) {
    BufferedImage rotatedImage = new BufferedImage(normalImage.getWidth(), normalImage.getHeight(), BufferedImage.TYPE_INT_ARGB);
    for (int x = 0; x < normalImage.getWidth(); x++) {
      for (int y = 0; y < normalImage.getHeight(); y++) {
        rotatedImage.setRGB(x, y, normalImage.getRGB(normalImage.getWidth() - x - 1, y));
      }
    }
    return rotatedImage;
  }

  public Player(float posX, float posY) {
    images = new LinkedList<>();
    rotatedImages = new LinkedList<>();
    images.add(i1);
    images.add(i2);
    images.add(i3);
    images.add(i4);
    rotatedImages.add(rotetedI1);
    rotatedImages.add(rotetedI2);
    rotatedImages.add(rotetedI3);
    rotatedImages.add(rotetedI4);
    rec = new Rectangle();
    x = posX;
    y = posY;
    startX = x;
    startY = y;
    sx = i.getWidth();
    sy = i.getHeight();
    dir = 1;
  }

  public static int getImageWidth() {
    return i.getWidth();
  }

  @Override
  public void fire() {
    Game.map.addMissele(new Missile(x, y + getSy() / 2, dir));
  }

  @Override
  void paint(Graphics g) {
//      setSizeAndPos(rec);
//      g.setColor(Color.RED);
//      g.drawRect(rec.x, rec.y, rec.width, rec.height);
    if (Game.window.getController().isMoving() && !(speedY < -((4.02f) / Window.getFPS()) || speedY > ((4.02f) / Window.getFPS())) && !(Game.map.isEnd())) {
      int time = (int) (((System.currentTimeMillis() * 8) / 1000) % 4);
      if (dir == 1) {
        g.drawImage(images.get(time), (int) x, (int) (y + Game.map.getOffsetY()), Game.window);
      } else {
        g.drawImage(rotatedImages.get(time), (int) x, (int) (y + Game.map.getOffsetY()), Game.window);
      }
    } else {
      if (dir == 1) {
        g.drawImage(i, (int) x, (int) (y + Game.map.getOffsetY()), Game.window);
      } else {
        g.drawImage(rotetedI, (int) x, (int) (y + Game.map.getOffsetY()), Game.window);
      }
    }
  }

  /**
   * Nastavi rozmery obdelnika podle hrace pro zjisteni kolize
   * @param a obdelnik pro zjisteni kolize
   */
  @Override
  public void setSizeAndPos(Rectangle a) {

    if (dir == 1) {
      a.width = (int) (sx - 20);
      a.x = (int) (x + 20);
    } else {
      a.width = (int) (sx - 20);
      a.x = (int) x;
    }
    a.height = (int) sy;
    a.y = (int) (y + Game.map.getOffsetY());
  }

  @Override
  public void move(int mx, int my) {
    //pokud ma hrac skocit zjisti se kolize se zemi aby neskakal ve vzduchu
    if (my < 0) {
      y += 1;
      setSizeAndPos(rec);
      if (Game.map.hasCollision(rec, true)) {
        speedY = -2.5f;
      }
      y -= 1;
      setSizeAndPos(rec);

    }
    //nastaveni smeru
    float fmx = mx * speed, fmy = speedY * speed;
    if (mx > 0) {
      dir = 1;
    }
    if (mx < 0) {
      dir = -1;
    }

    boolean playerMoveX = true;
    boolean mapMoveX = true;

    //zjistovani kolize s mapou
    if ((Game.map.getOffsetX() > 0 && dir == -1) || ((Game.map.getOffsetX() + Game.map.getMapWidth()) <= Game.window.getWidth() && dir == 1)) {
      mapMoveX = false;
    }

    if (dir == -1) {
      if ((x + sx) <= (int) ((1.0f / 3.0f) * (float) Game.window.getWidth())) {

        playerMoveX = false;
        //pokud nejde hejbat s mapou hrac dojde dokonce
        if (!mapMoveX) {
          if (x > 0) {
            playerMoveX = true;
          }
        }
      } else {
        mapMoveX = false;
      }
    }

    if (dir == 1) {
      if (x >= (int) ((2.0f / 3.0f) * (float) Game.window.getWidth())) {
        playerMoveX = false;
        if (!mapMoveX) {
          if ((x + sx) < Game.window.getWidth()) {
            playerMoveX = true;
          }
        }
      } else {
        mapMoveX = false;
      }
    }


    //kolize s mapou
    if (playerMoveX) {
      x += fmx / Window.getFPS();

      setSizeAndPos(rec);
      if (Game.map.hasCollision(rec, true)) {
        x -= fmx / Window.getFPS();
        setSizeAndPos(rec);
      }
    }
    if (mapMoveX) {
      Game.map.setOffsetX(Game.map.getOffsetX() - fmx / Window.getFPS());
      setSizeAndPos(rec);
      if (Game.map.hasCollision(rec, true)) {
        Game.map.setOffsetX(Game.map.getOffsetX() + fmx / Window.getFPS());

        setSizeAndPos(rec);
      }
    }
    y += fmy / Window.getFPS();
    setSizeAndPos(rec);
    if (Game.map.hasCollision(rec, true)) {
      y -= fmy / Window.getFPS();
      speedY = 0;
      setSizeAndPos(rec);
    }
    //zvysovani rychlosti kvuli skoku
    speedY += (3.02f) / Window.getFPS();
    if (speedY > 3) {
      speedY = 3;
    }
  }

  @Override
  void collision(Figure figure) {
    if (figure instanceof Monster || figure instanceof Bird) {
      Game.window.getStats().setLifes(Game.window.getStats().getLifes() - 1);
      x = startX;
      y = startY;
      Game.map.setOffsetX(0);
    }
  }

  @Override
  void hit() {
    Game.map.figures.remove(this);
  }
}
