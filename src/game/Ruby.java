package game;

import javax.swing.ImageIcon;

/**
 * Item na urceny na zbirani a ziskavani score.
 * @author Zuzana Vilhelmova
 */
public class Ruby extends Item {

  static ImageIcon i = new ImageIcon(Dirt.class.getResource("ruby.png"));

  public Ruby(float posX, float posY) {
    this.posX = posX;
    this.posY = posY;
    image = i.getImage();
    sizeX = image.getWidth(Game.window);
    sizeY = image.getHeight(Game.window);
  }

  @Override
  void collision(Figure figure) {
    synchronized (Game.map.items) {
      if (figure instanceof Player) {
        Game.map.items.remove(this);
        Game.window.getStats().setScore(Game.window.getStats().getScore() + 10);
      }
    }
  }
}
