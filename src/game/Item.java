package game;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

/**
 * abstraktni trida urcena pro predmety na sbirani.
 * @author Zuzana Vilhelmova
 */
public abstract class Item {

  protected float posX, posY;
  protected int sizeX;
  protected int sizeY;
  protected Image image;

  public Item() {
  }

  public void setSizeAndPos(Rectangle a) {
    a.width = (int) sizeX;
    a.height = (int) sizeY;
    a.x = (int) (posX + Game.map.getOffsetX());
    a.y = (int) (posY + Game.map.getOffsetY());
  }

  public void paint(Graphics g) {
    g.drawImage(image, (int) (posX + Game.map.getOffsetX()), (int) (posY + Game.map.getOffsetY()), Game.window);
  }


  abstract void collision(Figure figure);
}
