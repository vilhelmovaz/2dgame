package game;

import java.awt.Rectangle;
import java.util.LinkedList;
import java.util.List;

/**
 * Trida zjistujici kolize.
 * @author Zuzana Vilhelmova
 */
public class Collider {

    private Rectangle a, b;

    public Collider() {
        a = new Rectangle();
        b = new Rectangle();
    }

    /**
     * Projde postavy a zjisti kolize s ostatnimy postavy, itemy a strely.
     */
    public void update() {
        List<Figure> removedFigures = new LinkedList<>();
        for (Figure figureA : Game.map.figures) {
            figureA.setSizeAndPos(a);
            for (Figure figureB : Game.map.figures) {
                if (figureA == figureB) {
                    continue;
                }
                figureB.setSizeAndPos(b);
                if (a.intersects(b)) {
                    figureA.collision(figureB);
                }
                  
            }
            List<Item> removedItems = new LinkedList<>();
            synchronized (Game.map.items) {
                for (Item item : Game.map.items) {
                    item.setSizeAndPos(b);
                    if (a.intersects(b)) {
                        removedItems.add(item);

                    }
                }
                for (Item item : removedItems) {
                    item.collision(figureA);
                }

            }
            
            if (figureA instanceof Monster || figureA instanceof Bird) {
                for (Missile missile : Game.map.missiles) {
                    missile.setSizeAndPos(b);
                    if (a.intersects(b)) {
                        removedFigures.add(figureA);
                        missile.setRemove(true);
                    }
                }
                
            }
        }
        for(Figure figure: removedFigures){
            figure.hit();
        }
        for (Missile missile : Game.map.missiles) {
            missile.setSizeAndPos(a);
            if (Game.map.hasCollision(a, false)) {
                missile.setRemove(true);
            }
        }
    }
}
