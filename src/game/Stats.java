package game;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

/**
 * Trida vykreslujici score, zivoty, level, prohru/vyhru a tlacitka 
 * @author Zuzana Vilhelmova
 */
public class Stats implements MouseListener, MouseMotionListener {

  private int lifes;
  private int score = 0;
  private int maxScore = 0;
  private float percent = 0;
  private Point point = new Point();
  private Button buttonNext = null;
  private Button buttonMenu = null;
  private Button buttonBack = null;

  public Stats() {
    buttonMenu = new Button(50, 26);
    buttonNext = new Button(80, 26);
    buttonBack = new Button(120, 26);
  }

  public int getLifes() {
    return lifes;
  }

  public int getScore() {
    return score;
  }

  public void setMaxScore(int maxScore) {
    this.maxScore = maxScore;
  }

  public void setLifes(int lifes) {
    this.lifes = lifes;
  }

  public void setScore(int score) {
    this.score = score;
  }

  private float countPercent() {
    float x = (100.0f / (float) maxScore) * (float) score;
    System.out.println("(100/" + maxScore + ")*" + score + " = " + x);
    return x;
  }

  /**
   * Pokud neni konec hry, vykresluje score, zivoty a level. Pokud je pauza,
   * vykresli napis PAUSE. Pokud je zmakly escape, vykresli se tlacitko do menu
   * a zpet do hry. Pokud dojdou zivoty vykresli se Game over a tlacitko do menu.
   * Pokud se dostal hrac do cile vykresli se dosazene skore z maxima, procenta
   * dosazeneho score a tlacitka do menu a dalsiho levelu, pokud neni posledni.
   * @param g 
   */
  public void paint(Graphics g) {
    g.setColor(Color.RED);
    g.setFont(new Font("Monospaced", Font.BOLD, 30));
    if (Game.map.getLevel() > 0) {
      String stringlevel = Integer.toString(Game.map.getLevel());
      g.drawString("Level: " + stringlevel, Game.window.getWidth() / 2, 80);
    } else {
      g.drawString("Level: " + Game.window.getMenu().getFile().getName(), Game.window.getWidth() / 2, 80);
    }

    if (!Game.map.isEnd()) {
      g.setColor(Color.RED);
      g.setFont(new Font("Monospaced", Font.BOLD, 25));
      String stringScore = Integer.toString(score);
      String stringLifes = Integer.toString(lifes);
      g.drawString("SCORE: " + stringScore, 20, 55);
      g.drawString("LIFES: " + stringLifes, 20, 80);
      if (Game.window.getController().isPause()) {
        g.setFont(new Font("Monospaced", Font.BOLD, 60));
        int w = g.getFontMetrics().stringWidth("PAUSE");
        g.drawString("PAUSE", Game.window.getWidth() / 2 - w / 2, Game.window.getHeight() / 2);
      }
      if (lifes < 1 || Game.window.getController().isQuit()) {
        Game.map.setEnd(true);
        Game.window.end();
      }
    } else {
      if (lifes < 1) {
        g.setColor(Color.RED);
        g.setFont(new Font("Monospaced", Font.PLAIN, 50));
        int w1 = g.getFontMetrics().stringWidth("You lost!");
        g.drawString("You lost!", Game.window.getWidth() / 2 - w1 / 2, Game.window.getHeight() / 2 - 70);
        g.setFont(new Font("Monospaced", Font.BOLD, 60));
        String msg = "Game over";
        int w = g.getFontMetrics().stringWidth(msg);
        g.drawString(msg, Game.window.getWidth() / 2 - w / 2, Game.window.getHeight() / 2);
        buttonMenu.paint(g, Game.window.getWidth() / 2 - buttonMenu.getSizeX() / 2, Game.window.getHeight() / 2 - buttonMenu.getSizeY(), "Menu", 15);
      } else {
        if (!Game.window.getController().isQuit()) {
          g.setFont(new Font("Monospaced", Font.BOLD, 60));
          percent = countPercent() * 100;
          percent = (int) percent;
          percent /= 100;
          String stringPercent = Float.toString(percent);
          System.out.println(stringPercent);
          String stringScore = Integer.toString(score);
          String stringMaxScore = Integer.toString(maxScore);
          int w1 = g.getFontMetrics().stringWidth("You win!");
          g.drawString("You win!", Game.window.getWidth() / 2 - w1 / 2, Game.window.getHeight() / 2 - 70);
          g.setFont(new Font("Monospaced", Font.PLAIN, 50));
          int w = g.getFontMetrics().stringWidth("SCORE: " + stringScore + "/" + stringMaxScore + " (" + stringPercent + "%)");
          g.drawString("SCORE: " + stringScore + " / " + stringMaxScore + " (" + stringPercent + "%)", Game.window.getWidth() / 2 - w / 2, Game.window.getHeight() / 2);
          System.out.println("level: " + Game.map.getLevel());
          if (Game.map.getLevel() < 5 && Game.map.getLevel() != 0) {
            buttonNext.paint(g, Game.window.getWidth() / 2 + 50, Game.window.getHeight() / 2 - buttonNext.getSizeY(), "Next lvl", 15);
          }
          buttonMenu.paint(g, Game.window.getWidth() / 2 - 100, Game.window.getHeight() / 2 - buttonMenu.getSizeY(), "Menu", 15);
        } else {
          buttonMenu.paint(g, Game.window.getWidth() / 2 - 100, Game.window.getHeight() / 2 - buttonMenu.getSizeY(), "Menu", 15);
          buttonBack.paint(g, Game.window.getWidth() / 2 + 50, Game.window.getHeight() / 2 - buttonBack.getSizeY(), "Back to Game", 15);
        }
      }
    }
  }

  @Override
  public void mouseClicked(MouseEvent e) {
    point.x = e.getX();
    point.y = e.getY();
    if (buttonNext.getRec().contains(point) && lifes >= 1 && Game.map.getLevel() != 0) {
      if (Game.map.getLevel() < 5) {
        Game.map.setLevel(Game.map.getLevel() + 1);
        Game.map.reset();
      }

    
    }
    if (buttonMenu.getRec().contains(point)) {
      Game.window.endGame();
    }
    
    if (buttonBack.getRec().contains(point)) {
      Game.window.getController().setQuit(false);
      Game.window.start();
      Game.map.setEnd(false);
    }
  }

  @Override
  public void mousePressed(MouseEvent e) {
  }

  @Override
  public void mouseReleased(MouseEvent e) {
  }

  @Override
  public void mouseEntered(MouseEvent e) {
  }

  @Override
  public void mouseExited(MouseEvent e) {
  }

  @Override
  public void mouseDragged(MouseEvent e) {
  }

  @Override
  public void mouseMoved(MouseEvent e) {
    point.x = e.getX();
    point.y = e.getY();

    if (buttonMenu.getRec().contains(point)) {
      buttonMenu.setColor(Color.YELLOW);
      buttonMenu.setFontColor(Color.BLACK);
    } else {
      if (buttonMenu.getColor() != Color.BLACK) {
        buttonMenu.setColor(Color.BLACK);
        buttonMenu.setFontColor(Color.WHITE);
      }
    }
    
    if (buttonNext.getRec().contains(point)) {
      buttonNext.setColor(Color.YELLOW);
      buttonNext.setFontColor(Color.BLACK);
    } else {
      if (buttonNext.getColor() != Color.BLACK) {
        buttonNext.setColor(Color.BLACK);
        buttonNext.setFontColor(Color.WHITE);
      }
    }

    if (buttonBack.getRec().contains(point)) {
      buttonBack.setColor(Color.YELLOW);
      buttonBack.setFontColor(Color.BLACK);
    } else {
      if (buttonBack.getColor() != Color.BLACK) {
        buttonBack.setColor(Color.BLACK);
        buttonBack.setFontColor(Color.WHITE);
      }
    }

  }
}
