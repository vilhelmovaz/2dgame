package game;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 * Trida predstavujici strelu hrace.
 * @author Zuzana Vilhelmova
 */
public class Missile {

    private float x, y;
    private final float MISSILE_SPEED = 400;
    private int dir;
    private boolean remove = false;
    private Rectangle rec;
    static BufferedImage i;
    static BufferedImage rotetedI;
    
    static{
      try {
        i = ImageIO.read(Missile.class.getResource("missile.png"));
      } catch (IOException ex) {
        Logger.getLogger(Missile.class.getName()).log(Level.SEVERE, null, ex);
      }
      rotetedI = getRotated(i);
    }

    public Missile(float x, float y, int dir) {
        rec = new Rectangle();
        this.x = x;
        this.y = y;
        this.dir = dir;
    }

    /**
     * Nastavi rozmery obdelnika podle objektu pro zjisteni kolize
     * @param a obdelnik pro zjisteni kolize
     */
     public void setSizeAndPos(Rectangle a) {
        a.width = (int) i.getWidth(Game.window);
        a.height = (int) i.getHeight(Game.window);
        a.x = (int) (x);
        a.y = (int) (y+Game.map.getOffsetY());
    }
    
    private static BufferedImage getRotated(BufferedImage normalImage) {
    BufferedImage rotatedImage = new BufferedImage(normalImage.getWidth(), normalImage.getHeight(), BufferedImage.TYPE_INT_ARGB);
    for (int x = 0; x < normalImage.getWidth(); x++) {
      for (int y = 0; y < normalImage.getHeight(); y++) {
        rotatedImage.setRGB(x, y, normalImage.getRGB(normalImage.getWidth() - x - 1, y));
      }
    }
    return rotatedImage;
  }

    public void paint(Graphics g) {
      if(dir==1){
        g.drawImage(i, (int) x + Player.getImageWidth()/2, (int) (y+Game.map.getOffsetY()), Game.window);
      }else{
        g.drawImage(rotetedI, (int) x, (int) (y+Game.map.getOffsetY()), Game.window);
      }
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public void move() {
        x += (MISSILE_SPEED * dir)/Window.getFPS();
        setSizeAndPos(rec);
        if (Game.map.hasCollision(rec, false)) {
            remove = true;
        }
        if (x >= Game.window.getWidth() || x < 0) {
            remove = true;
        }
    }

    public void setRemove(boolean remove) {
        this.remove = remove;
    }

    public boolean isRemove() {
        return remove;
    }
}
