package game;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.LinkedList;
import java.util.List;

/**
 * Trida ovlada hrace a updatuje pozice objektu.
 * @author Zuzana Vilhelmova
 */
public class Controller implements KeyListener {

  private boolean up;
  private boolean right;
  private boolean left;
  private boolean killing;
  private boolean moving = false;
  private boolean pause = false;
  private boolean quit = false;
  private long lastMissile = 0;

  public Controller() {
  }

  public boolean isMoving() {
    return moving;
  }

  public boolean isRight() {
    return right;
  }

  public boolean isLeft() {
    return left;
  }

  public boolean isPause() {
    return pause;
  }

  public boolean isQuit() {
    return quit;
  }

  public void setQuit(boolean quit) {
    this.quit = quit;
  }

  /**
   * updatuje pozici postav, strel a oblacku.
   * Pokud se maji smazat da je do tempListu a pote smaze.
   * Pokud hra zkoncila, je pauza nebo zmacknuty escape, nastavi se up, right,
   * left, moving a killing na false.
   */
  public void update() {
    if (!Game.map.isEnd() && !pause && !quit) {
      for (Figure figure : Game.map.figures) {
        int x = 0, y = 0;
        if (up) {
          y = -1;
        }
        if (right) {
          x = 1;
        }
        if (left) {
          x = -1;
        }
        if (killing) {
          if ((System.currentTimeMillis() - lastMissile) > 300) {
            fire();
            lastMissile = System.currentTimeMillis();
          }
        }
        figure.move(x, y);
      }

      synchronized (Game.map.missiles) {
        List<Missile> tempList = new LinkedList<>();
        for (Missile missile : Game.map.missiles) {
          missile.move();
          if (missile.isRemove()) {
            tempList.add(missile);
          }
        }

        for (Missile missile : tempList) {
          Game.map.missiles.remove(missile);
        }
      }
      synchronized (Game.map.clouds) {
        List<Cloud> tempList = new LinkedList<>();
        for (Cloud cloud : Game.map.clouds) {
          cloud.move();
          if (cloud.getLeftX() >= cloud.getRightX()) {
            tempList.add(cloud);
          }
        }

        for (Cloud cloud : tempList) {
          Game.map.clouds.remove(cloud);
        }
      }
    } else {
      up = false;
      right = false;
      left = false;
      killing = false;
      moving = false;
    }
  }

  public void fire() {
    for (Figure figure : Game.map.figures) {
      figure.fire();
    }
  }

  @Override
  public void keyTyped(KeyEvent e) {
  }

  @Override
  public void keyPressed(KeyEvent e) {
    int key = e.getKeyCode();

    if (key == KeyEvent.VK_CONTROL) {
      killing = true;
    }
    if (key == KeyEvent.VK_LEFT || key == KeyEvent.VK_A) {
      left = true;
      moving = true;
    }
    if (key == KeyEvent.VK_RIGHT || key == KeyEvent.VK_D) {
      right = true;
      moving = true;
    }
    if (key == KeyEvent.VK_SPACE) {
      up = true;

    }
  }

  @Override
  public void keyReleased(KeyEvent e) {
    int key = e.getKeyCode();
    if (key == KeyEvent.VK_P || key == KeyEvent.VK_PAUSE) {
      if (pause == true) {
        pause = false;
      } else {
        pause = true;
      }
    }
    if (key == KeyEvent.VK_ESCAPE) {
      System.out.println(quit);
      quit = true;
    }
    if (key == KeyEvent.VK_CONTROL) {
      killing = false;
    }
    if (key == KeyEvent.VK_LEFT || key == KeyEvent.VK_A) {
      left = false;
      moving = false;
    }
    if (key == KeyEvent.VK_RIGHT || key == KeyEvent.VK_D) {
      right = false;
      moving = false;
    }
    if (key == KeyEvent.VK_SPACE) {
      up = false;
    }
  }
}
