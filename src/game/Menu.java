package game;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.File;
import javax.swing.JFileChooser;

/**
 * Trida vykresluje menu.
 * @author Zuzana Vilhelmova
 */
public class Menu implements MouseListener, MouseMotionListener {

  private Button buttonStart;
  private Button buttonQuit;
  private Button buttonHelp;
  private Button buttonLoad;
  private Button buttonBack;
  private Point point;
  private File file;
  private int fontSize = 30;
  private int buttonSizeX = 320;
  private int buttonSizeY = 45;

  public Menu() {
    point = new Point();
    point.x = 0;
    point.y = 0;
    buttonStart = new Button(buttonSizeX, buttonSizeY);
    buttonQuit = new Button(buttonSizeX, buttonSizeY);
    buttonHelp = new Button(buttonSizeX, buttonSizeY);
    buttonLoad = new Button(buttonSizeX, buttonSizeY);
    buttonBack = new Button(buttonSizeX, buttonSizeY);
    buttonStart.setColor(Color.RED);
    buttonStart.setFontColor(Color.GREEN);
    buttonLoad.setColor(Color.RED);
    buttonLoad.setFontColor(Color.GREEN);
    buttonBack.setColor(Color.RED);
    buttonBack.setFontColor(Color.GREEN);
    buttonQuit.setColor(Color.RED);
    buttonQuit.setFontColor(Color.GREEN);
    buttonHelp.setColor(Color.RED);
    buttonHelp.setFontColor(Color.GREEN);
  }

  public void paint(Graphics g) {
    g.setColor(Color.ORANGE);
    g.setFont(new Font("Monospaced", Font.BOLD, 80));
    int w = g.getFontMetrics().stringWidth("MENU");
    if (Game.window != null) {
      g.drawString("MENU", Game.window.getWidth() / 2 - w / 2, 130);
      buttonStart.paint(g, Game.window.getWidth() / 2 - buttonSizeX / 2, 150, "START", fontSize);
      if (Game.map.isOwnLevel()) {
        buttonBack.paint(g, Game.window.getWidth() / 2 - buttonSizeX / 2, 230, "LOAD BACK DEFAULT", fontSize);
      } else {
        buttonLoad.paint(g, Game.window.getWidth() / 2 - buttonSizeX / 2, 230, "LOAD OWN LEVEL", fontSize);
      }

      buttonHelp.paint(g, Game.window.getWidth() / 2 - buttonSizeX / 2, 310, "HELP", fontSize);
      buttonQuit.paint(g, Game.window.getWidth() / 2 - buttonSizeX / 2, 390, "QUIT", fontSize);
      g.setColor(Color.YELLOW);
      g.setFont(new Font("Monospaced", Font.PLAIN, 13));
      int w1 = g.getFontMetrics().stringWidth("Author: Zuzana Vilhelmová");
      g.drawString("Author: Zuzana Vilhelmová", 30, Game.window.getHeight() - 15);
    }
  }

  public File getFile() {
    return file;
  }

  @Override
  public void mouseClicked(MouseEvent e) {
    point.x = e.getX();
    point.y = e.getY();

    if (buttonStart.getRec().contains(point)) {
      Game.window.startGame();
    }
    if (buttonLoad.getRec().contains(point) && !Game.map.isOwnLevel()) {

      JFileChooser fc = new JFileChooser();
      int res = fc.showOpenDialog(Game.window);
      if (res == JFileChooser.APPROVE_OPTION) {
        file = fc.getSelectedFile();
        Game.map.setOwnLevel(true);
      }
    }
    if (buttonBack.getRec().contains(point) && Game.map.isOwnLevel()) {
      Game.map.setOwnLevel(false);
      file = null;
    }
    if (buttonHelp.getRec().contains(point)) {
      Game.window.startHelp();
    }
    if (buttonQuit.getRec().contains(point)) {
      System.exit(0);
    }
  }

  @Override
  public void mousePressed(MouseEvent e) {
  }

  @Override
  public void mouseReleased(MouseEvent e) {
  }

  @Override
  public void mouseEntered(MouseEvent e) {
  }

  @Override
  public void mouseExited(MouseEvent e) {
  }

  @Override
  public void mouseDragged(MouseEvent e) {
  }

  @Override
  public void mouseMoved(MouseEvent e) {
    point.x = e.getX();
    point.y = e.getY();

    if (buttonStart.getRec().contains(point)) {
      buttonStart.setColor(Color.YELLOW);
      buttonStart.setFontColor(Color.RED);
    } else {
      if (buttonStart.getColor() != Color.RED) {
        buttonStart.setColor(Color.RED);
        buttonStart.setFontColor(Color.GREEN);
      }
    }

    if (buttonLoad.getRec().contains(point)) {
      buttonLoad.setColor(Color.YELLOW);
      buttonLoad.setFontColor(Color.RED);
    } else {
      if (buttonLoad.getColor() != Color.RED) {
        buttonLoad.setColor(Color.RED);
        buttonLoad.setFontColor(Color.GREEN);
      }
    }

    if (buttonBack.getRec().contains(point)) {
      buttonBack.setColor(Color.YELLOW);
      buttonBack.setFontColor(Color.RED);
    } else {
      if (buttonBack.getColor() != Color.RED) {
        buttonBack.setColor(Color.RED);
        buttonBack.setFontColor(Color.GREEN);
      }
    }

    if (buttonHelp.getRec().contains(point)) {
      buttonHelp.setColor(Color.YELLOW);
      buttonHelp.setFontColor(Color.RED);
    } else {
      if (buttonHelp.getColor() != Color.RED) {
        buttonHelp.setColor(Color.RED);
        buttonHelp.setFontColor(Color.GREEN);
      }
    }

    if (buttonQuit.getRec().contains(point)) {
      buttonQuit.setColor(Color.YELLOW);
      buttonQuit.setFontColor(Color.RED);
    } else {
      if (buttonQuit.getColor() != Color.RED) {
        buttonQuit.setColor(Color.RED);
        buttonQuit.setFontColor(Color.GREEN);
      }
    }
  }
}
