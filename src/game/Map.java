package game;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Nacita a vykresluje mapu.
 * @author Zuzana Vilhelmova
 */
public class Map {

  public List<Item> items = null;
  public List<Figure> figures = null;
  public List<Missile> missiles = null;
  public List<Cloud> clouds = null;
  private float offsetX;
  private float offsetY = 0;
  private long[][] array;
  private Player player;
  private Stone stone;
  private Dirt dirt;
  private Grass grass;
  private Ruby ruby;
  private Diamond diamond;
  private Rectangle rec;
  final static int WIDTH = 120;
  final static int HEIGHT = 15;
  private boolean end = false;
  private int level;
  private boolean ownLevel = false;

  public Map() {
    dirt = new Dirt();
    stone = new Stone();
    grass = new Grass();
    array = new long[WIDTH][HEIGHT];
    rec = new Rectangle();
    figures = new LinkedList<>();
    missiles = new LinkedList<>();
    items = new LinkedList<>();
    clouds = new LinkedList<>();
    level = 1;
  }

  public void addMissele(Missile missile) {
    missiles.add(missile);
  }

  public void addToList(Figure figure) {
    figures.add(figure);
  }

  public void addItem(Item item) {
    items.add(item);
  }

  public float getOffsetX() {
    return offsetX;
  }

  public float getOffsetY() {
    return offsetY;
  }

  public boolean isOwnLevel() {
    return ownLevel;
  }

  public void setOwnLevel(boolean ownLevel) {
    this.ownLevel = ownLevel;
  }
  

  public int getMapWidth() {
    return WIDTH * 40;
  }

  public boolean isEnd() {
    return end;
  }

  public void setEnd(boolean end) {
    this.end = end;
  }

  public void setOffsetX(float offsetX) {
    this.offsetX = offsetX;
  }

  public void setOffsetY(float offsetY) {
    this.offsetY = offsetY;
  }

  public void setLevel(int level) {
    this.level = level;
  }

  public int getLevel() {
    return level;
  }

  /**
   * Zjistuje kolize s mapou a s koncem
   * @param a obdelnik predstavujici hrace nebo strelu pro zjisteni kolize
   * @param player boolean predstavujici hodnotu zda se jedna o hrace
   * @return true pokud je kolize s mapou, jinak false
   */
  public boolean hasCollision(Rectangle a, boolean player) {
    boolean collision = false;
    for (int i = 0; i < WIDTH; i++) {
      for (int j = 0; j < HEIGHT; j++) {
        rec.height = (int) Block.BLOCK_SIZE;
        rec.width = (int) Block.BLOCK_SIZE;
        rec.x = (int) (i * Block.BLOCK_SIZE + offsetX);
        rec.y = (int) (j * Block.BLOCK_SIZE + offsetY);
        if (rec.intersects(a) && BlockType.isEnd(array[i][j]) && player) {
          end = true;
          Game.window.end();
        }
        if (rec.intersects(a) && (array[i][j] == 1 || array[i][j] == 2 || array[i][j] == 3)) {
          collision = true;
        }
      }
    }
    return collision;
  }

  public void paint(Graphics g) {
    for (int i = 0; i < WIDTH; i++) {
      for (int j = 0; j < HEIGHT; j++) {
        switch (BlockType.getType(array[i][j])) {
          case 1:
            grass.paint(g, i * grass.BLOCK_SIZE + offsetX, j * grass.BLOCK_SIZE + offsetY);
            continue;
          case 2:
            dirt.paint(g, i * dirt.BLOCK_SIZE + offsetX, j * dirt.BLOCK_SIZE + offsetY);
            continue;
          case 3:
            stone.paint(g, i * stone.BLOCK_SIZE + offsetX, j * stone.BLOCK_SIZE + offsetY);
            continue;
        }
      }
    }
    synchronized (missiles) {
      for (Missile missile : missiles) {
        missile.paint(g);
      }
    }
    synchronized (items) {
      for (Item item : items) {
        item.paint(g);
      }
    }
    synchronized (clouds){
      for(Cloud cloud : clouds){
        cloud.paint(g);
      }
    }
  }
  
  public void read(Scanner scanner){
          for (int i = 0; i < WIDTH; i++) {
        for (int j = 0; j < HEIGHT; j++) {
          array[i][j] = scanner.nextLong();
        }
      }
      for (int i = 0; i < WIDTH; i++) {
        for (int j = 0; j < HEIGHT; j++) {
          if (BlockType.getType(array[i][j]) == 6) {
            long temp = array[i][j];
            array[i][j] = array[i][j - 1];
            array[i][j - 1] = temp;
            break;
          }
        }
      }
  }
  
  /**
   * Nacte vlastni soubor s levelem
   * @param file 
   */
  public void load(File file){
    try {
      Scanner scanner = new Scanner(file);
      read(scanner);
    } catch (FileNotFoundException ex) {
      Logger.getLogger(Map.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  /**
   * Nacte soubor s levlem
   * @param fileName 
   */
  public void load(String fileName) {
    try {
      Scanner scanner = new Scanner(Map.class.getResource(fileName).openStream());
      read(scanner);
    } catch (IOException ex) {
      Logger.getLogger(Map.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  public void reset() {
    int monster = 0;
    int bird = 0;
    int ruby = 0;
    int diamond = 0;
    end = false;
    offsetX = 0;
    offsetY = 0;
    clouds.add(new Cloud(4 * dirt.BLOCK_SIZE, 100, 3));
    if (Game.window != null) {
      Game.window.getStats().setScore(0);
      if(level==1 || level ==0){
        Game.window.getStats().setLifes(2);
      }
      if (level > 1) {
        Game.window.getStats().setLifes(Game.window.getStats().getLifes() + 1);
        Game.window.start();
      }
      items.clear();
      figures.clear();
      
    }
    switch (level) {
      case 0:
        load(Game.window.getMenu().getFile());
        break;
      case 1:
        load("level1.lvl");
        break;
      case 2:
        load("level2.lvl");
        break;
      case 3:
        load("level3.lvl");
        break;
      case 4:
        load("level4.lvl");
        break;
      case 5:
        load("level5.lvl");
        break;
    }

    int id = 0;
    boolean found = false;
    for (int i = 0; i < WIDTH; i++) {
      for (int j = 0; j < HEIGHT; j++) {
        switch (BlockType.getType(array[i][j])) {
          case 4:
            ruby++;
            addItem(new Ruby(i * dirt.BLOCK_SIZE, j * dirt.BLOCK_SIZE));
            continue;
          case 5:
            diamond++;
            addItem(new Diamond(i * dirt.BLOCK_SIZE, j * dirt.BLOCK_SIZE));
            continue;
          case 6:
            addToList(new Player(i * dirt.BLOCK_SIZE, j * dirt.BLOCK_SIZE));
            continue;
          case 7:           
            id = BlockType.getMonsterId(array[i][j]);
            found = false;
            for (Figure figure : figures) {
              if (figure instanceof Monster) {
                if (((Monster) figure).getId() == id) {
                  found = true;
                  ((Monster) figure).setRightX(i * dirt.BLOCK_SIZE);
                  ((Monster) figure).setRightY(j * dirt.BLOCK_SIZE);
                  if (((Monster) figure).getRightX() < ((Monster) figure).getLeftX()) {
                    System.out.println("switching");
                    float tempX = ((Monster) figure).getRightX();
                    ((Monster) figure).setRightX(((Monster) figure).getLeftX());
                    ((Monster) figure).setLeftX(tempX);
                  }
                  break;
                }
              }

            }
            if (!found) {
              monster++;
              addToList(new Monster(i * dirt.BLOCK_SIZE, j * dirt.BLOCK_SIZE, id));
            }


            continue;
          case 8:      
            id = BlockType.getMonsterId(array[i][j]);
            found = false;
            for (Figure figure : figures) {
              if (figure instanceof Bird) {
                if (((Bird) figure).getId() == id) {
                  found = true;
                  ((Bird) figure).setRightX(i * dirt.BLOCK_SIZE);
                  ((Bird) figure).setRightY(j * dirt.BLOCK_SIZE);
                  if (((Bird) figure).getRightX() < ((Bird) figure).getLeftX()) {
                    float tempX = ((Bird) figure).getRightX();
                    ((Bird) figure).setRightX(((Bird) figure).getLeftX());
                    ((Bird) figure).setLeftX(tempX);
                  }
                  break;
                }
              }
            }
            if (!found) {
              bird++;
              addToList(new Bird(i * dirt.BLOCK_SIZE, j * dirt.BLOCK_SIZE, id));
            }
            continue;
        }
      }
    }
    Game.window.getStats().setMaxScore(monster*20+bird*30+ruby*10+diamond*100);
  }
}
