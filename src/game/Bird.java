package game;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 * Trida predstavujici ptaka zabijejiciho hrace.
 * @author Zuzana Vilhelmova
 */
public class Bird extends Figure {

  private float leftX, leftY, rightX, rightY;
  private int id;
  final static int speed = 90;
  public int dir = 1;
  static BufferedImage i;
  static BufferedImage i1;
  static BufferedImage i2;
  static BufferedImage rotetedI;
  static BufferedImage rotetedI1;
  static BufferedImage rotetedI2;

  static {
    try {
      i = ImageIO.read(Monster.class.getResource("bird.png"));
      i1 = ImageIO.read(Monster.class.getResource("bird1.png"));
      i2 = ImageIO.read(Monster.class.getResource("bird2.png"));
    } catch (IOException ex) {
      Logger.getLogger(Monster.class.getName()).log(Level.SEVERE, null, ex);
    }
    rotetedI = getRotated(i);
    rotetedI1 = getRotated(i1);
    rotetedI2 = getRotated(i2);
  }

  public Bird(float fromX, float fromY, int id) {
    images = new LinkedList<>();
    rotatedImages = new LinkedList<>();
    images.add(i);
    images.add(i1);
    images.add(i2);
    rotatedImages.add(rotetedI);
    rotatedImages.add(rotetedI1);
    rotatedImages.add(rotetedI2);
    leftX = fromX;
    leftY = fromY;
    this.id = id;
    x = fromX;
    y = fromY;
    sx = i.getWidth();
    sy = i.getHeight();
  }

  public void setRightX(float rightX) {
    this.rightX = rightX;
  }

  public void setRightY(float rightY) {
    this.rightY = rightY;
  }

  public int getId() {
    return id;
  }

  public float getLeftX() {
    return leftX;
  }

  public float getRightX() {
    return rightX;
  }

  public void setLeftX(float leftX) {
    this.leftX = leftX;
  }

  private static BufferedImage getRotated(BufferedImage normalImage) {
    BufferedImage rotatedImage = new BufferedImage(normalImage.getWidth(), normalImage.getHeight(), BufferedImage.TYPE_INT_ARGB);
    for (int x = 0; x < normalImage.getWidth(); x++) {
      for (int y = 0; y < normalImage.getHeight(); y++) {
        rotatedImage.setRGB(x, y, normalImage.getRGB(normalImage.getWidth() - x - 1, y));
      }
    }
    return rotatedImage;
  }

  @Override
  void paint(Graphics g) {
    if (!Game.map.isEnd() && !Game.window.getController().isPause() && !Game.window.getController().isQuit()) {
      int time = (int) (((System.currentTimeMillis() * 8) / 1000) % 3);
      if (dir == 1) {
        g.drawImage(images.get(time), (int) (x + Game.map.getOffsetX()), (int) (y + Game.map.getOffsetY()), Game.window);
      } else {
        g.drawImage(rotatedImages.get(time), (int) (x + Game.map.getOffsetX()), (int) (y + Game.map.getOffsetY()), Game.window);
      }
    } else {
      if (dir == 1) {
        g.drawImage(i, (int) (x + Game.map.getOffsetX()), (int) (y + Game.map.getOffsetY()), Game.window);
      } else {
        g.drawImage(rotetedI, (int) (x + Game.map.getOffsetX()), (int) (y + Game.map.getOffsetY()), Game.window);
      }
    }
  }

  @Override
  void hit() {
    Game.window.getStats().setScore(Game.window.getStats().getScore() + 30);
    Game.map.figures.remove(this);
  }

  /**
   * Zjisteni kolize a v pripade kolize obraceni smeru.
   * @param figure 
   */
  @Override
  void collision(Figure figure) {
    if (figure instanceof Bird) {
      dir *= -1;
      figure.setDir(this);
    }
  }

  @Override
  public void move(int mx, int my) {
    float fmx = speed;
    if ((dir == 1 && x > rightX) || (dir == -1 && x < leftX)) {
      dir *= -1;
    }
    x += (fmx * dir) / Window.getFPS();
  }
}
